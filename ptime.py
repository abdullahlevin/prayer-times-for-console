#!/usr/bin/env python3
import requests, datetime, pytz

# http://islamicfinder.us/index.php/api/prayer_times?longitude=28.68&latitude=41.07&timezone=Asia/Riyadh&method=3
def get_prayer_time(lon=28.68, lat=41.07, timezone="Asia/Riyadh", method=3):
    data = {"longitude": lon, "latitude": lat, "timezone": timezone, "method": method}
    r = requests.get("http://islamicfinder.us/index.php/api/prayer_times", params=data)
    pray = []
    for k, v in r.json()["results"].items():
        pray.append(v)
    return [x.replace("%", "") for x in pray]


if __name__ == "__main__":
    # TODO: add timeleft to next prayer and how many prayers left today
    # TODO: make timezone been taken from system
    # TODO: to make command save location
    # TODO: make command let you choose method
    # TODO: write help for command
    # TODO: make less dependencies
    now = datetime.datetime.now(tz=pytz.timezone("Asia/Riyadh"))
    now = now.strftime("%H:%M")
    results = [
        datetime.datetime.strptime(result, "%I:%M %p") for result in get_prayer_time()
    ]

    clean = dict(zip(["Fajr", "Sunrise", "Zuhr", "Asr", "Maghrib", "Isha"], results))
    print("Now:", now)
    for k, v in clean.items():
        if datetime.datetime.strptime(now, "%H:%M").time() < v.time():
            print(k, v.strftime("%H:%M"))
